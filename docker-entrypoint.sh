#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
# or first arg is `something.conf`
#if [ "${1#-}" != "$1" ] || [ "${1%.conf}" != "$1" ]; then
#	set -- redis-server "$@"
#fi

# allow the container to be started with `--user`
if [ "$1" = 'gitproxy' -a "$(id -u)" = '0' ]; then
	chown -R gitproxy .
	exec su-exec gitproxy "$0" "$@"
fi

if [ "$1" = 'gitproxy' ]; then
    set -- spawn-fcgi -n -a 0.0.0.0 -p 9000 -- /usr/bin/fcgiwrap
fi

exec "$@"
