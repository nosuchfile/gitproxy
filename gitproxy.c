/*
 * Copyright (c) 2016, Gregory Thiemonge
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/stat.h>

int main(void)
{
    char *path_info = getenv("PATH_INFO");
    char *p;
    char *q;
    char *args[] = {
        "git-http-backend",
        NULL,
    };

    if(path_info == NULL) {
        exit(EXIT_FAILURE);
    }

    p = strstr(path_info, ":/");
    if(p) {
        setenv("PATH_INFO", p + 1, 1);
    }

    setenv("GIT_PROJECT_ROOT", GIT_PROJECT_ROOT, 1);
    setenv("GIT_HTTP_EXPORT_ALL", "1", 1);

    if((q = strstr(path_info, "/info/refs")) != NULL) {
        char reponame[PATH_MAX];
        char dirname[PATH_MAX];
        struct stat buf;

        strncpy(reponame, path_info + 1, q - path_info - 1);
        reponame[q - path_info - 1] = '\0';

        p = strstr(reponame, ":/");
        if(p == NULL) {
            exit(EXIT_FAILURE);
        }
        sprintf(dirname, "%s/%s", GIT_PROJECT_ROOT, p + 2);

        if(stat(dirname, &buf) < 0) {

            char cmd[PATH_MAX];
            sprintf(cmd, "git clone --mirror --bare %.*s://%s %s",
                    (int)(p - reponame), reponame, p + 2, dirname);

            //fprintf(stderr, "Calling %s\n", cmd);
            if(system(cmd) == 0) {
                sprintf(cmd, "GIT_DIR=%s git update-server-info", dirname);

                //fprintf(stderr, "Calling %s\n", cmd);
                system(cmd);
            }
        }
    }

    if(execv(GIT_HTTP_BACKEND, args) < 0) {
        perror("execv");
    }

    return 0;
}
