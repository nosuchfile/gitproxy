gitproxy
========

A CGI application to proxify GIT protocol (git://) requests over HTTP.

Dependencies
------------

* git
* spawn-fcgi
- fcgiwrap

Usage
-----

```shell
$ make
$ spawn-fcgi -n -a 127.0.0.1 -p 9000 -- /usr/bin/fcgiwrap
```

Configure nginx to send requests to spawn-fcgi:

```
[..]
    location ~ /gitproxy(/.*) {
        include /etc/nginx/fastcgi_params;

        fastcgi_param SCRIPT_FILENAME /path/to/gitproxy;
        fastcgi_param PATH_INFO $1;

        fastcgi_pass localhost:9000;
    }
[..]
```

To clone a git:// repository over HTTP:

```shell
$ git clone http://myserver/gitproxy/git://githost/repo.git
```

Or use ~/.gitconfig to automatically handle git:// requests:
```
[url "http://myserver/gitproxy/git:/"]
    insteadOf = git://
```

Then:

```shell
$ git clone git://githost/repo.git
```

```
