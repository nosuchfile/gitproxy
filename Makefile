GIT_HTTP_BACKEND = /usr/libexec/git-core/git-http-backend
GIT_PROJECT_ROOT = $(PWD)/repositories/

CFLAGS=-O2 -Wall -Wextra \
       -DGIT_PROJECT_ROOT="\"$(GIT_PROJECT_ROOT)\"" \
       -DGIT_HTTP_BACKEND="\"$(GIT_HTTP_BACKEND)\""

all: gitproxy

gitproxy: gitproxy.o

clean:
	$(RM) gitproxy gitproxy.o

docker-image:
	docker build -t gitproxy .

.PHONY: clean docker-image
