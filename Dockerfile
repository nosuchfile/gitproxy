# docker run --name=gitproxy -d -p 127.0.0.1:9000:9000 -v $(pwd)/data:/data gitproxy
FROM alpine:3.4

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN addgroup -S gitproxy && adduser -S -G gitproxy gitproxy

# grab su-exec for easy step-down from root
RUN apk add --no-cache 'su-exec>=0.2'

RUN apk add --no-cache spawn-fcgi fcgiwrap git

COPY gitproxy.c Makefile /opt/
RUN set -x \
	&& apk add --no-cache --virtual .build-deps \
		gcc \
		linux-headers \
		make \
		musl-dev \
	&& make -C /opt GIT_PROJECT_ROOT="\"/data\"" GIT_HTTP_BACKEND="\"/usr/libexec/git-core/git-http-backend\"" \
	&& apk del .build-deps \
    && rm -f /opt/gitproxy.c /opt/Makefile

RUN mkdir /data && chown gitproxy:gitproxy /data
VOLUME /data
WORKDIR /data

COPY docker-entrypoint.sh /opt/
ENTRYPOINT ["/opt/docker-entrypoint.sh"]

EXPOSE 9000
CMD [ "gitproxy" ]
